#!/usr/bin/python -tt
import sys
 
def Cat(filename):
  f = open(filename, 'rU')
  text = f.read()
  print text
  f.close()
# Define a main() function that prints a little greeting.
def main():
  # Get the name from the command line, using 'World' as a fallback.
  Cat(sys.argv[1])
 
# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main()